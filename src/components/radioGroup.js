import * as React from "react";
import {
    View,
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  List,
  ListItem,
    Radio,
    Toast
} from "native-base";

export interface Props {
}

export interface State {
  selected: any
}

const styles = {
    disabledRow: {
        opacity: 0.3
    },

    enabledRow: {
        opacity: 1
    },
}

class RadioGroup extends React.Component<Props, State> {

  constructor(props) {
      super(props);
      this.state = {selected: ''};
  }

  static getDerivedStateFromProps(nextProps) {
    const { selected } = nextProps;
    return {
        selected: selected
    };
  }

  render() {
    const {items, inactiveIds, onChange, isActive} = this.props;
    return (
      <View style={styles.container}>
        {items && items.map((valObj, i)=>{
          return <ListItem
              key={`item-${i}`}
              onPress={() => {
                  if (isActive && inactiveIds.indexOf(valObj.id) === -1) {
                      onChange(valObj.id);
                  }
              }}
              selected={this.state.selected === valObj.id}
          >
              <Left style={isActive && inactiveIds.indexOf(valObj.id) === -1 ? styles.enabledRow : styles.disabledRow}>
                  <Text>{valObj.value}</Text>
              </Left>
              <Right style={isActive &&  inactiveIds.indexOf(valObj.id) === -1? styles.enabledRow : styles.disabledRow}>
                  <Radio
                      selected={this.state.selected === valObj.id}
                  />
              </Right>
          </ListItem>;
        })}
      </View>
    );
  }
}

export default (RadioGroup);
