import * as React from "react";
import {
    View,
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  List,
  ListItem,
    Radio,
    Toast
} from "native-base";
import RadioGroup from "../../../components/radioGroup";
import { union } from "lodash";

import styles from "./styles";
export interface Props {
  navigation: any;
  list: any;
}
export interface State {
  selected: any
}

const getInactiveIds = (keys, selected) => {
    let arr = [];
    selected.map((key, index)=>{
        if (keys[key]) {
            arr = union(arr, keys[key]);
        }
    });
    return arr;
};

class Home extends React.Component<Props, State> {

  constructor(props) {
      super(props);
      this.state = {selected: []};
  }

  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.navigate("DrawerOpen")}
              />
            </Button>
          </Left>
          <Body>
            <Title>Tick Tick Toe</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <List>
            {this.props.list.form && this.props.list.form.map((item, index) => {
                return <View key={`group${index}`}>
                    <ListItem itemDivider>
                        <Text>{index + 1}</Text>
                    </ListItem>
                    <RadioGroup selected={this.state.selected[index]}
                                items={item}
                                inactiveIds={getInactiveIds(this.props.list.validator, this.state.selected)}
                                onChange={(id)=>{
                                    let temp = [...this.state.selected.splice(0, index)];
                                    temp[index] = id;
                                    this.setState({selected: temp});
                                }}
                                isActive={index === 0 || this.state.selected[index-1]}
                    />
                  </View>;
            })}
          </List>
          <Button full disabled={ (this.props.list.form && !this.state.selected[this.props.list.form.length-1]) } onPress={()=>{
              Toast.show({
                  text: JSON.stringify(this.state.selected),
                  buttonText: "Okay",
                  duration: 5000
              })
          }}>
              <Text>Submit</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export default Home;
